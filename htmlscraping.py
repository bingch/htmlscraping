#!/usr/bin/python
import getopt, sys
from lxml import html
import requests

def usage():
    print("Usage: " + sys.argv[0] + " [-t tag ] [-s SEARCHING_STR] URL")
    print("default tag is script")


def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "ht:s:u:", ["help", "search=", "url=", "tag="])
    except getopt.GetoptError as err:
        # print help information and exit:
        print(err)  # will print something like "option -a not recognized"
        usage()
        sys.exit(1)
    search_str = None
    search_url = None
    tag = 'script'
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-t", "--tag"):
            tag = a
        elif o in ("-s", "--search"):
            search_str = a
        else:
            assert False, "unhandled option"
    if len(args) < 1:
        usage()
        sys.exit(3)

    for search_url in args:
        #print( "=" * int((40 -  len(search_url) / 2)) + search_url + "=" * int(40 - len(search_url) / 2))
        
        page = requests.get(search_url)
        tree = html.fromstring(page.content)
    
        tag_contents = tree.xpath('//' + tag + '/text()')
        for content in tag_contents:
            if search_str:
                if search_str.casefold() not in content.casefold():
                    pass
                else:
                    print(content)
            else:
                print(content)


if __name__ == "__main__":
    main()
